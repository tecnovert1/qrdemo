FROM rust:1.60-alpine as build

RUN apk add libc-dev

RUN mkdir qrdonate
WORKDIR /qrdonate


COPY ./Cargo.toml ./Cargo.toml
COPY ./src ./src
COPY ./static ./static
COPY ./templates ./templates


RUN RUSTFLAGS='-C link-arg=-s' cargo build --target x86_64-unknown-linux-musl --release

FROM alpine

# binary must be two folders deep, or it will hang using 100% cpu
RUN mkdir -p /data/site

COPY --from=build /qrdonate/target/x86_64-unknown-linux-musl/release/qrdonate /data/site/
COPY --from=build /qrdonate/static/ /data/site/static
COPY --from=build /qrdonate/templates/ /data/site/templates

WORKDIR /data/site

CMD ["./qrdonate"]
