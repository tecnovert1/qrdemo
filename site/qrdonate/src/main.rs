// Copyright (c) 2022 tecnovert
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

use std::env;
use std::net::TcpStream;
use std::io::{Read, Write};
use std::{thread, time};
use std::path::Path;
use std::fs::OpenOptions;

use actix_web::{
    error, http, middleware::{ErrorHandlerResponse, ErrorHandlers, Logger},
    dev, App, HttpRequest, HttpServer, HttpResponse, Result, web::{self, Data}
};
use actix_files::{Files, NamedFile};
use actix_web_lab::web as web_lab;
use tera::{Context, Tera};
use urlencoding;

extern crate qrcodegen;
use qrcodegen::QrCode;
use qrcodegen::QrCodeEcc;

extern crate dirs;
use qstring::QString;


struct State {
    rpc_host: String,
    rpc_port: i32,
    rpc_username: String,
    rpc_password: String,
    rpc_wallet: String,
    address_from: String,
    address_to: String,
    donate_amount: f64,
    site_url: String,
}

// Returns a string of SVG code for an image depicting
// the given QR Code, with the given number of border modules.
// The string always uses Unix newlines (\n), regardless of the platform.
fn to_svg_string(qr: &QrCode, border: i32) -> String {
    assert!(border >= 0, "Border must be non-negative");
    let mut result = String::new();
    //result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    //result += "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
    let dimension = qr.size().checked_add(border.checked_mul(2).unwrap()).unwrap();
    result += &format!(
        "<svg width=\"50%\" height=\"50%\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 {0} {0}\" stroke=\"none\">\n", dimension);
    result += "\t<rect width=\"100%\" height=\"100%\" fill=\"#FFFFFF\"/>\n";
    result += "\t<path d=\"";
    for y in 0 .. qr.size() {
        for x in 0 .. qr.size() {
            if qr.get_module(x, y) {
                if x != 0 || y != 0 {
                    result += " ";
                }
                result += &format!("M{},{}h1v1h-1z", x + border, y + border);
            }
        }
    }
    result += "\" fill=\"#000000\"/>\n";
    result += "</svg>\n";
    result
}

fn call_rpc(
    host: &String, port: i32, wallet: &String,
    username: &String, password: &String,
    method: &String, params: &json::JsonValue) -> Result<json::JsonValue, String> {

    let mut stream = match TcpStream::connect(format!("{}:{}", host, port)) {
        Ok(rv) => rv,
        Err(err) => return Err(err.to_string()),
    };

    let mut content = String::new();
    content.push_str("{\"method\": \"");
    content.push_str(&method);
    content.push_str("\", \"params\": ");
    content.push_str(&json::stringify(params.clone()));
    content.push_str(", \"id\": 1}\n");

    let mut request = String::new();
    if wallet.is_empty() {
        request += "POST / HTTP/1.1\n";
    } else {
        request += &format!("POST /wallet/{} HTTP/1.1\n", urlencoding::encode(&wallet));
    }
    request += &format!("Host: {}\n", host);
    request += &format!("Content-Length: {}\n", content.len());
    request += "Connection: close\n";
    request += &format!("Authorization: Basic {}\n", base64::encode(format!("{}:{}", username, password)));
    request += "\n\r";
    request += &content;
    //println!("request {}", request);

    stream.write(request.as_bytes()).expect("Write failed");

    let mut response = String::new();
    match stream.read_to_string(&mut response) {
        Ok(_) => {},
        Err(err) => return Err(err.to_string()),
    };
    //println!("response {}", response);

    let v: Vec<&str> = response.split("\n\r").collect(); // Split header and body on CRLF
    let message_body = v[1];
    let mut parsed = match json::parse(&message_body) {
        Ok(rv) => rv,
        Err(err) => return Err(format!("Failed to parse JSON: {}\n", err.to_string()).to_string()),
    };

    if !parsed["error"].is_null() {
        return Err(parsed["error"].to_string());
    }

    return Ok(parsed.remove("result"));
}

async fn index(tmpl: web::Data<Tera>, data: Data<State>, _req: HttpRequest) -> Result<HttpResponse> {
    //println!("REQ: {:?}", req);

    let mut context = Context::new();
    context.insert("fromaddress", &data.address_from);
    context.insert("toaddress", &data.address_to);
    println!("data.address_from {}", data.address_from);
    println!("data.address_from {}", data.address_to);


    let rendered = tmpl
        .render("index.html.tera", &context)
        .map_err(error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(rendered))
}

async fn qr_code(_tmpl: web::Data<Tera>, data: Data<State>, req: HttpRequest) -> Result<HttpResponse> {
    println!("REQ: {:?}", req);
    // Trim id string if too long
    let qs = QString::from(req.query_string());
    let mut qr_id = qs.get("id").unwrap_or("");
    if qr_id.chars().count() > 32 {
        let mut end : usize = 0;
        qr_id.chars().into_iter().take(32).for_each(|x| end += x.len_utf8());
        qr_id = &qr_id[..end]
    }

    let text = if qr_id != "" {
        format!("{}?id={}", &data.site_url, qr_id)
    } else {
        data.site_url.clone()
    };

    let errcorlvl: QrCodeEcc = QrCodeEcc::Low;
    let qr: QrCode = QrCode::encode_text(&text, errcorlvl).unwrap();

    let mut s = String::new();
    s += "<!DOCTYPE html><html lang=\"en\">";
    s += "<head><meta charset=\"UTF-8\"></head><body>";
    s += &to_svg_string(&qr, 2);
    s += "<p>";
    s += &text;
    s += "</p>";
    s += "</body></html>";

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

async fn response(tmpl: web::Data<Tera>, data: Data<State>, req: HttpRequest) -> Result<HttpResponse> {
    let qs = QString::from(req.query_string());
    let txid = qs.get("txid").unwrap_or("");

    let mut context = Context::new();
    context.insert("amount", &data.donate_amount);
    context.insert("recipient", "Somewhere");
    context.insert("txid", &txid);

    let rendered = tmpl
        .render("scan_response.html.tera", &context)
        .map_err(error::ErrorInternalServerError)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(rendered))
}

async fn scan(tmpl: web::Data<Tera>, data: Data<State>, req: HttpRequest) -> Result<HttpResponse> {
    //println!("REQ: {:?}", req);

    /* TODO
        1. How to specify json directly?
        2. Select inputs explicitly from outputs to address_from
    */
    let params_str = format!("[\"part\", \"part\", \
            [{{\"address\": \"{}\", \"amount\": {:.8}}}], \
            \"\", \"\", 8, 1, false, \
            {{\"changeaddress\": \"{}\"}}]", &data.address_to, &data.donate_amount, &data.address_from);
    let params = json::parse(&params_str).unwrap();

    let mut rv = json::JsonValue::new_object();
    let mut error = "".to_string();

    match call_rpc(&data.rpc_host, data.rpc_port, &data.rpc_wallet, &data.rpc_username, &data.rpc_password,
                   &"sendtypeto".to_string(), &params) {
        Ok(rvi) => {
            rv = rvi
        },
        Err(err) => {
            println!("RPC Error: {}", err.to_string());
            error = err.to_string();
        }
    }

    if !error.is_empty() {
        let mut user_text1 = "An unknown error has occurred".to_string();
        let mut user_text2 = "".to_string();

        if error.contains("Insufficient funds") {
            user_text1 = "The donation wallet has run out of funds.".to_string();
            user_text2 = "Consider refilling it at address: ".to_string() + &data.address_from.to_string();
        }

        let mut context = Context::new();
        context.insert("text1", &user_text1);
        context.insert("text2", &user_text2);
        let rendered = tmpl
            .render("scan_error.html.tera", &context)
            .map_err(error::ErrorInternalServerError)?;

        return Ok(HttpResponse::Ok().content_type("text/html").body(rendered))
    } else {
        let txid = &rv.as_str().unwrap();

        // Trim id string if too long
        let qs = QString::from(req.query_string());
        let mut qr_id = qs.get("id").unwrap_or("");
        if qr_id.chars().count() > 32 {
            let mut end : usize = 0;
            qr_id.chars().into_iter().take(32).for_each(|x| end += x.len_utf8());
            qr_id = &qr_id[..end]
        }

        let log_message = format!("Sent tx {}, scanned with id {}", &txid, &qr_id);
        println!("{}", &log_message);

        let home_path = dirs::home_dir().unwrap();
        let log_path = Path::new(&home_path).join("qrdemo.log");
        let mut file = OpenOptions::new()
            .create(true)
            .write(true)
            .append(true)
            .open(log_path)
            .unwrap();
        writeln!(file, "{}", &log_message).expect("Failed to write to log");

        // Redirect so page refresh won't double submit
        return Ok(HttpResponse::Found().append_header((http::header::LOCATION, format!("/response?txid={}", txid))).finish())
    }
}

pub fn bad_request<B>(res: dev::ServiceResponse<B>) -> Result<ErrorHandlerResponse<B>> {
    let error_msg: String = match res.response().error() {
        Some(e) => format!("{:?}", e),
        None =>  String::from("Unknown Error")
    };
    println!("bad_request: {}", error_msg);
    let new_resp = NamedFile::open("static/errors/400.html")?
        .set_status_code(res.status())
        .into_response(res.request())
        .map_into_right_body();
    Ok(ErrorHandlerResponse::Response(res.into_response(new_resp)))
}

pub fn not_found<B>(res: dev::ServiceResponse<B>) -> Result<ErrorHandlerResponse<B>> {
    let error_msg: String = match res.response().error() {
        Some(e) => format!("{:?}", e),
        None =>  String::from("Unknown Error")
    };
    println!("not_found: {}", error_msg);
    let new_resp = NamedFile::open("static/errors/404.html")?
        .set_status_code(res.status())
        .into_response(res.request())
        .map_into_right_body();
    Ok(ErrorHandlerResponse::Response(res.into_response(new_resp)))
}

pub fn internal_server_error<B>(res: dev::ServiceResponse<B>) -> Result<ErrorHandlerResponse<B>> {
    let error_msg: String = match res.response().error() {
        Some(e) => format!("{:?}", e),
        None =>  String::from("Unknown Error")
    };
    println!("internal_server_error: {}", error_msg);
    let new_resp = NamedFile::open("static/errors/500.html")?
        .set_status_code(res.status())
        .into_response(res.request())
        .map_into_right_body();
    Ok(ErrorHandlerResponse::Response(res.into_response(new_resp)))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    let rpc_host = env::var("RPC_HOST").expect("$RPC_HOST is not set");
    let rpc_port = env::var("RPC_PORT").expect("$RPC_PORT is not set").parse::<i32>().unwrap();

    let rpc_username = env::var("RPC_USERNAME").expect("$RPC_USERNAME is not set");
    let rpc_password = env::var("RPC_PASSWORD").expect("$RPC_PASSWORD is not set");
    let rpc_wallet = env::var("RPC_WALLET").expect("$RPC_WALLET is not set");

    let site_url = env::var("URL").expect("$URL is not set");
    let address_to = env::var("ADDRESS_TO").expect("$ADDRESS_TO is not set");

    let donate_amount = env::var("DONATE_AMOUNT").unwrap_or("0.1".to_string()).parse::<f64>().unwrap();

    let http_host = env::var("HTTP_HOST").unwrap_or("127.0.0.1".to_string()).to_string();
    let http_port = env::var("HTTP_PORT").unwrap_or("8000".to_string()).parse::<u16>().unwrap();

    println!("RPC settings {}:{}", rpc_host, rpc_port);

    println!("Waiting for daemon connection...");
    let mut params = json::JsonValue::new_object();
    let mut rv;
    loop {
        match call_rpc(&rpc_host, rpc_port, &rpc_wallet, &rpc_username, &rpc_password,
                       &"getblockchaininfo".to_string(), &params) {
            Ok(rvi) => {
                rv = rvi;
                break;
            },
            Err(err) => {
                println!("RPC Error: {}", err.to_string());
                thread::sleep(time::Duration::from_secs(1));
            }
        }
    }
    println!("Blocks: {}", rv["blocks"].as_i32().unwrap());

    println!("Waiting for wallet...");
    loop {
        match call_rpc(&rpc_host, rpc_port, &rpc_wallet, &rpc_username, &rpc_password,
                       &"getwalletinfo".to_string(), &params) {
            Ok(rvi) => {
                rv = rvi;
                break;
            },
            Err(err) => {
                println!("RPC Error: {}", err.to_string());
                thread::sleep(time::Duration::from_secs(1));
            }
        }
    }
    println!("Wallet Balance: {}", rv["total_balance"].as_f64().unwrap());

    params = json::JsonValue::Array(vec![json::JsonValue::from(0i32), json::JsonValue::from(0i32)]);
    rv = call_rpc(&rpc_host, rpc_port, &rpc_wallet, &rpc_username, &rpc_password,
                  &"deriverangekeys".to_string(), &params).unwrap();
    let address_from = rv[0].to_string();
    println!("address_from {}", address_from);

    // Disable staking if necessary
    params = json::JsonValue::Array(vec![json::JsonValue::from("stakingoptions")]);
    rv = call_rpc(&rpc_host, rpc_port, &rpc_wallet, &rpc_username, &rpc_password,
                  &"walletsettings".to_string(), &params).unwrap();

    if rv["stakingoptions"]["enabled"].as_bool().unwrap_or(true) {
        println!("Disabling staking");
        let opts = json::parse("{\"enabled\": false}").unwrap();
        params = json::JsonValue::Array(vec![json::JsonValue::from("stakingoptions"), opts]);
        rv = call_rpc(&rpc_host, rpc_port, &rpc_wallet, &rpc_username, &rpc_password,
                      &"walletsettings".to_string(), &params).unwrap();
        assert!(!rv["stakingoptions"]["enabled"].as_bool().unwrap());
    }

    let appstate = Data::new(State {
        rpc_host: rpc_host,
        rpc_port: rpc_port,
        rpc_username: rpc_username,
        rpc_password: rpc_password,
        rpc_wallet: rpc_wallet,
        address_from: address_from,
        address_to: address_to,
        donate_amount: donate_amount,
        site_url: site_url,
    });

    println!("Serving at {}:{}", http_host, http_port);
    HttpServer::new(move || {

        let mut templates = Tera::new("templates/**/*").expect("errors in tera templates");
        templates.autoescape_on(vec!["tera"]);

        let error_handlers = ErrorHandlers::new()
            .handler(
                http::StatusCode::INTERNAL_SERVER_ERROR,
                internal_server_error,
            )
            .handler(http::StatusCode::BAD_REQUEST, bad_request)
            .handler(http::StatusCode::NOT_FOUND, not_found);

        App::new()
            .app_data(web::Data::new(templates))
            .app_data(Data::clone(&appstate))
            .wrap(Logger::default())
            .wrap(error_handlers)
            .service(web::resource("/qr").to(qr_code))
            .service(web::resource("/scan").to(scan))
            .service(web::resource("/response").to(response))
            .service(web::resource("/").to(index))
            .service(web_lab::Redirect::new("/favicon.ico", "/static/favicon.ico"))
            .service(Files::new("/static", "static/"))
    })
    .bind((http_host, http_port))?
    .run()
    .await
}
